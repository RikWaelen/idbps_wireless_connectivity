#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>
#include <MPU9250.h>

RF24 radio(7,8);                    // nRF24L01(+) radio CS and CSN pins 

RF24Network network(radio);          // Network uses that radio

MPU9250 mpu;

const uint16_t this_node = 03;        // Address of this node in Octal format
const uint16_t master = 00;       // Address of the master node in Octal format

struct payload_t {                 // Structure of the payload sent to RX
	int16_t x_roll;
	int16_t y_pitch;
	int16_t z_yaw;
};

int16_t x,y,z; // Variables to save measured data in before sending

void setup(void)
{
	Serial.begin(115200); // Open serial for debugging


	//Setup necessary for MPU9250 sensor
	Wire.begin(); 
	delay(1000);
	mpu.setup();
	
	//Setup necessary for NRF Radio
	SPI.begin();
	radio.begin();
	radio.setPALevel(RF24_PA_LOW);
	radio.setDataRate(RF24_2MBPS);
	
	//Opening the network with chosen channel and node nr.
	network.begin(90,this_node);

}

void loop() {

	network.update(); //Check the network regularly if there is still activity

	//Update the MPU sensor and get the XYZ Gyro values
	mpu.update();
	x = mpu.getRoll();
	y = mpu.getPitch();
	z = mpu.getYaw();
	
	//Save the measurements in the payload to sent to master
	payload_t payload = { x,y,z};
	
	//Send measurements to master with our own header and payload
	RF24NetworkHeader header(master);    
	network.write(header,&payload,sizeof(payload));
}
