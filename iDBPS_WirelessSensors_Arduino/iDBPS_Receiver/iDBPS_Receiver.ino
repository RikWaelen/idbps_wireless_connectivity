#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>
#include <SoftwareSerial.h>

SoftwareSerial BLE(2, 3);		// HM-10 bluetooth module TX, RX
RF24 radio(9, 10);               // nRF24L01(+) radio CS and CSN pins

RF24Network network(radio);      // Network uses that radio
const uint16_t master = 00;    // Address of our node in Octal format ( 04,031, etc)

struct payload_t {                 // Structure of the payload received from TX
	int16_t x_roll;
	int16_t y_pitch;
	int16_t z_yaw;
};

payload_t payload;

// Initialising variables needed with 1 or 0
int16_t x_offset[3] = {1, 1, 1};
int16_t y_offset[3] = {1, 1, 1};
int16_t z_offset[3] = {1, 1, 1};

int16_t sensors[3][3] = {{1, 1, 1}, {1, 1, 1}, {1, 1, 1}};
int16_t sensorbuffer[10];

String sensornames[9] = {"A", "B", "C", "D", "E", "F", "G", "H", "I",};

int sensor_id = 0;

unsigned long currentMillis = 0;
unsigned long previousMillis = 0;
unsigned int serialInterval = 50;

void setup(void)
{
	//Open serial connection with COM and Bluetooth module
	Serial.begin(115200);
	BLE.begin(115200);

	Serial.println("Serial opened.");

	//Open connection to the NRF radio
	SPI.begin();
	radio.begin();

	//Save settings for NRF radio
	radio.setPALevel(RF24_PA_LOW);
	radio.setDataRate(RF24_2MBPS);
	
	//Open network of NRF radios with the chosen channel and address of this node
	network.begin(/*channel*/ 90, /*node address*/ master);

	/* ---- Calibrating to 0, creating offset ---- 
	Using the first value received from each sensor to define an offset 
	If both sensors have been received once, continue to loop*/
	while (x_offset[0] == 1 || x_offset[1] == 1) {
		network.update();                  // Check the network regularly for messages
		while ( network.available() ) {     // Is there anything ready for us?

			RF24NetworkHeader header;        // If so, grab it and save it in payload
			network.read(header, &payload, sizeof(payload));

			sensor_id = (header.from_node) - 1;

			x_offset[sensor_id] = payload.x_roll;
			y_offset[sensor_id] = payload.y_pitch;
			z_offset[sensor_id] = payload.z_yaw;
		}
	}

}

void loop(void) {

	network.update();                  // Check the network regularly
	while ( network.available() ) {     // Is there anything ready for us?

		RF24NetworkHeader header;        // If so, grab it and print it out
		network.read(header, &payload, sizeof(payload));

		sensor_id = (header.from_node) - 1; //node number minus 1 to start with 0

		// Save received value in sensor array minus offset
		sensors[sensor_id][0] = payload.x_roll - x_offset[sensor_id]; 
		sensors[sensor_id][1] = payload.y_pitch - y_offset[sensor_id];
		sensors[sensor_id][2] = payload.z_yaw - z_offset[sensor_id];
		
		// Place received values in sensorbuffer to be able to cast to bytes for bluetooth
		for (int i = 1; i < 11; i = i + 3) {
			sensorbuffer[i] = sensors[(i - 1) / 3][0];
			sensorbuffer[i + 1] = sensors[(i - 1) / 3][1];
			sensorbuffer[i + 2] = sensors[(i - 1) / 3][2];
		}
		sensorbuffer[0] = 255;

		//------ Print to BLE transciever ----->
		// If sending to unity, comment out this line
		BLE.write((byte*) sensorbuffer , sizeof(sensorbuffer)); 

		//------ Print for Unity -------
		// If sending to bluetooth, comment out the lines below
		
		/*currentMillis = millis();
		if (currentMillis - previousMillis > serialInterval) {
		for (int i = 0; i < 3; i++) {
		Serial.println(sensornames[i] + ";" + String(sensors[i][0]) + ";" + String(sensors[i][1]) + ";" + String(sensors[i][2]));
		}
		previousMillis = currentMillis;
		}*/
	}
}
