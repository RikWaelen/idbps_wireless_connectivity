README ARDUINO SOFTWARE IDBPS WIRELESS SENSORS

Software developed in Arduino IDE v1.8.10

Software needed to use these programs:

- Arduino IDE (minimal v1.8.10)
- MPU9250 library by hideakitai https://github.com/hideakitai/MPU9250
- RF24 library by TMRh20 http://tmrh20.github.io/RF24/
- RF24Network library by TMRh20 http://tmrh20.github.io/RF24Network/

Add the libraries mentioned above to your Arduino IDE. If you don't know how, use this guide: https://www.arduino.cc/en/guide/libraries

To update sensors:

1. Open the sensor software in the Arduino IDE.
2. Edit the 'this_node' value to represent the node that needs to be updated using the numbers for Sensor A = 01, Sensor B = 02 etc etc.
3. Remove the sensorboard from the housing by removing the two screws, removing the cover and sliding the components out.
4. Disconnect the battery from the board for safety.
5. Connect the sensor using the USB to Serial adapter. Check to make sure the cable is connected the right way. (The easiest way is to make sure that DTR on the serial adapter is connected to DTR on the Arduino.)
6. In the Arduino IDE set the board to the 'Arduino Pro or Pro Mini' and choose the 'ATmega328p (3.3V, 8 MHz)' variant.
7. Upload the updated software to the Arduino using the upload button from the Arduino IDE.

To update the central receiver unit:

1. Open the receiver software in the Arduino IDE.
2. Make the changes necessary to the code.
3. Use a USB mini cable and connect to the USB port of the receiver (not the charging port, which is the USB micro port).
4. In the Arduino IDE set the board to the 'Arduino Pro or Pro Mini' and choose the 'ATmega328p (5V, 16 MHz)' variant. (The Arduino inside the receiver is an Arduino Nano but the chinese manufacturers uploaded a Pro Mini bootloader).
5. Upload the updated software to the Arduino using the upload button from the Arduino IDE.
