﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml.Controls;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using System.IO;
using System.Diagnostics;

namespace centrale_verwerkings_unit
{
    public sealed partial class MainPage : Page
    {
        public static readonly GattLocalCharacteristicParameters gattParameters = new GattLocalCharacteristicParameters
        {
            CharacteristicProperties = GattCharacteristicProperties.Read |
                               GattCharacteristicProperties.Notify,
            WriteProtectionLevel = GattProtectionLevel.Plain,
            UserDescription = "Characteristic"
        };

        public static readonly Guid ServiceUuid = Guid.Parse("513f6bda-d351-42cd-8635-c4d4cea4a619");
        public static readonly Guid ReadUuid = Guid.Parse("732a8b9d-ee90-495c-b346-082acb07f092");

        GattServiceProvider serviceProvider;
        GattLocalCharacteristic _readCharacteristic;
        List<GattSubscribedClient> _clients;

        public MainPage()
        {
            this.InitializeComponent();
            this.GattServer();
        }

        private async void GattServer()
        {
            await ServiceProviderInitAsync();
        }

        private async Task ServiceProviderInitAsync()
        {
            var isServiceSuccessfullyCreated = await CreateGattService();
            if (isServiceSuccessfullyCreated)
            {
                var isReadSuccessfullyCreated = await CreateReadCharacteristic();
                if (isReadSuccessfullyCreated)
                {
                    var advParameters = new GattServiceProviderAdvertisingParameters
                    {
                        IsDiscoverable = true,
                        IsConnectable = true
                    };
                    serviceProvider.StartAdvertising(advParameters);
                }
            }
            return;
        }
        private async Task<bool> CreateGattService()
        {
            var result = await GattServiceProvider.CreateAsync(ServiceUuid);
            if (result.Error == BluetoothError.Success)
            {
                serviceProvider = result.ServiceProvider;
                return true; 
            }
            return false;
        }
        private async Task<bool> CreateReadCharacteristic()
        {
            var characteristicResult = await serviceProvider.Service.CreateCharacteristicAsync(ReadUuid, gattParameters);
            if (characteristicResult.Error == BluetoothError.Success)
            {
                _readCharacteristic = characteristicResult.Characteristic;
                _readCharacteristic.ReadRequested += ReadCharacteristic_ReadRequested;
                _readCharacteristic.SubscribedClientsChanged += NotifyCharacteristic_SubscribedClientsChanged;

                return true;
            }
            return false;
        }

        private async void ReadCharacteristic_ReadRequested(GattLocalCharacteristic sender, GattReadRequestedEventArgs args)
        {
            var deferral = args.GetDeferral();
            Int16 i = 0;
            var values = new List<Int16> { (Int16)i, (Int16)(i + 10000), (Int16)(i + 20000) };

            var request = await args.GetRequestAsync();

            using (var writer = new DataWriter())
            {
                var listBytes = values.SelectMany(BitConverter.GetBytes).ToArray();
                writer.WriteBytes(listBytes);
                request.RespondWithValue(writer.DetachBuffer());
            }
            NotifyTester();
            deferral.Complete();
        }

        private async void NotifyValue(Int16 i)
        {
            var values = new List<Int16> { i, (Int16)(i + 10000), (Int16)(i + 20000) };
            using (var writer = new DataWriter())
            {
                writer.ByteOrder = ByteOrder.BigEndian;
                var listBytes = values.SelectMany(BitConverter.GetBytes).ToArray();
                writer.WriteBytes(listBytes);
                await _readCharacteristic.NotifyValueAsync(writer.DetachBuffer());
            }
        }

        private void NotifyCharacteristic_SubscribedClientsChanged(GattLocalCharacteristic sender, object args)
        {
            _clients = sender.SubscribedClients.ToList();
        }

        private async Task NotifyTester()
        {
            Int16 i = 0;

            while (i <= 100)
            {
                NotifyValue(i);
                i++;
            }
            return;
        }
    }
}
