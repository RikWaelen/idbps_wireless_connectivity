import React, { Suspense } from 'react';
import * as THREE from 'three';
import { Canvas } from 'react-three-fiber';
import Sensor from '../Main/Sensor';
// import BasicShapeCharacter from './3DModels/BasicShapeCharacter';
// import Stacy from './3DModels/Stacy';
import Gilbert from './3DModels/Gilbert';

class DataVisualisatie extends React.Component {
  constructor(props) {
    super(props);
    this.fakeUpdate = this.fakeUpdate.bind(this);
    this.state = {
      sensorData: [new THREE.Euler(), new THREE.Euler(), new THREE.Euler()]
    };
  }
  receiveConnectedDevice(device) {}

  updateData(parsedValue) {
    if (!parsedValue) {
      return;
    }
    var data = new Int16Array(parsedValue.buffer);
    if (data.length !== 10) {
      console.log('invalid data length');
      return;
    }

    var sensors = [new THREE.Euler(), new THREE.Euler(), new THREE.Euler()];
    var j = 0;
    for (var i = 1; i < data.length; i += 3) {
      var sensor = new Sensor();
      sensor.id = j;
      sensor.roll = data[i];
      sensor.pitch = data[i + 1];
      sensor.yaw = data[i + 2];
      sensors[sensor.id] = new THREE.Euler(
        THREE.Math.degToRad(sensor.roll),
        THREE.Math.degToRad(sensor.pitch),
        THREE.Math.degToRad(sensor.yaw)
      );
      j++;
    }
    this.setState({ sensorData: sensors });
  }
  activateTestMode() {
    setInterval(this.fakeUpdate, 10);
  }
  fakeUpdate() {
    var sensors = [
      new THREE.Euler(
        this.state.sensorData[0].x - 0.01,
        this.state.sensorData[0].y - 0.01,
        this.state.sensorData[0].z - 0.01
      ),
      new THREE.Euler(
        this.state.sensorData[1].x - 0.02,
        this.state.sensorData[1].y - 0.02,
        this.state.sensorData[1].z - 0.02
      ),
      new THREE.Euler(
        this.state.sensorData[2].x - 0.02,
        this.state.sensorData[2].y - 0.02,
        this.state.sensorData[2].z - 0.02
      )
    ];

    this.setState({ sensorData: sensors });
  }
  render() {
    return (
      <div className='canvas-border'>
        <Canvas
          onCreated={({ gl }) => {
            gl.shadowMap.enabled = true;
            gl.shadowMap.type = THREE.PCFSoftShadowMap;
          }}
          camera={{
            fov: 30,
            near: 0.1,
            far: 1000,
            position: [0, 8.25, 25],
            rotation: [-0.1, 0, 0]
          }}
        >
          <fog attach='fog' args={['#282C34', 30, 65]} />
          <ambientLight intensity={0.4} />
          <pointLight
            castShadow
            shadowMapWidth={1024}
            shadowMapHeight={1024}
            color={0xffffff}
            distance={1000}
            position={[5, 18, 10]}
          />
          {/* <Suspense fallback={<mesh />}>
            <BasicShapeCharacter sensorData={this.state.sensorData} />
          </Suspense> */}
          <Suspense fallback={<mesh />}>
            <Gilbert sensors={this.state.sensorData} />
          </Suspense>

          {/* <Suspense fallback={<mesh />}>
            <Stacy sensors={this.state.sensorData} />
          </Suspense> */}
          <mesh key='floor' position={[0, -0.5, 0]} receiveShadow>
            <boxBufferGeometry attach='geometry' args={[300, 1, 200]} />
            <meshStandardMaterial attach='material' color={['#758199']} />
          </mesh>
          <mesh key='backgroundWall' position={[0, 94, -10]} receiveShadow>
            <boxBufferGeometry attach='geometry' args={[300, 200, 1]} />
            <meshStandardMaterial attach='material' color={['#282C34']} />
          </mesh>
          <mesh
            key='prop'
            position={[-10, 0.5, -3]}
            rotation={[0, 0.9, 0]}
            castShadow
            receiveShadow
          >
            <boxBufferGeometry attach='geometry' args={[6, 2, 3]} />
            <meshStandardMaterial attach='material' color={['#158199']} />
          </mesh>
        </Canvas>
      </div>
    );
  }
}
export default DataVisualisatie;
