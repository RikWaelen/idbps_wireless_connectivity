import * as THREE from 'three';
import React, { useState, useMemo } from 'react';
import { useRender, useLoader } from 'react-three-fiber';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { TextureLoader } from 'three';
import { Math } from 'three';

export default function Stacy(props) {
  const [gltf, set] = useState();
  useMemo(() => new GLTFLoader().load('Stacy/stacy.glb', set), [
    'Stacy/stacy.glb'
  ]);
  const texture = useLoader(TextureLoader, 'Stacy/stacy.jpg');
  if (gltf && texture) {
    var model = gltf.scene.children[0].children[1];
    model.castShadow = true;
    var newMaterial = new THREE.MeshStandardMaterial({
      skinning: true
      //map: texture
    });
    model.material = newMaterial;
  }
  useRender(() => {
    if (props.sensors && model) {
      var shoulder = gltf.scene.getObjectByName('mixamorigLeftArm');
      var elbow = gltf.scene.getObjectByName('mixamorigLeftForeArm');
      var wrist = gltf.scene.getObjectByName('mixamorigLeftHand');
      shoulder.rotation.x = props.sensors[0].x;
      shoulder.rotation.y = props.sensors[0].y;
      shoulder.rotation.z = props.sensors[0].z;
      elbow.rotation.x = shoulder.rotation.x - props.sensors[1].x;
      elbow.rotation.y = shoulder.rotation.y - props.sensors[1].y;
      elbow.rotation.z = shoulder.rotation.z - props.sensors[1].z;
      wrist.rotation.x =
        shoulder.rotation.x - elbow.rotation.x - props.sensors[2].x;
      wrist.rotation.y =
        shoulder.rotation.y - elbow.rotation.y - props.sensors[2].y;
      wrist.rotation.z =
        shoulder.rotation.z - elbow.rotation.z - props.sensors[2].z;
    }
  });

  return gltf ? (
    <primitive
      object={gltf.scene.getObjectByName('Stacy')}
      scale={[0.05, 0.05, 0.05]}
      position={[7, -0.1, 0]}
      rotation={[Math.degToRad(90), 0, 0]}
    />
  ) : null;
}
