import * as THREE from 'three';
import React, { useRef } from 'react';
import { useRender } from 'react-three-fiber';

export default function BasicShapeCharacter(props) {
  const upperArmLength = 1.2;
  const lowerArmLength = 1.2;
  const handLength = 0.5;
  return (
    <group key='character' position={[-7, 6.1, 0]}>
      <mesh key='Head' position={[0, 1, 0]} castShadow>
        <sphereBufferGeometry attach='geometry' args={[0.75, 15, 15]} />
        <meshStandardMaterial attach='material' />
      </mesh>
      <Arm
        key='RightArm'
        side={1}
        offset={1.4}
        sensors={props.sensorData}
        upperArmLength={upperArmLength}
        lowerArmLength={lowerArmLength}
        handLength={handLength}
      />
      <Arm
        key='LeftArm'
        side={-1}
        offset={1.4}
        sensors={[new THREE.Euler(), new THREE.Euler(), new THREE.Euler()]}
        upperArmLength={upperArmLength}
        lowerArmLength={lowerArmLength}
        handLength={handLength}
      />
      <mesh key='Body' position={[0, -1.5, 0]} castShadow>
        <boxBufferGeometry attach='geometry' args={[2, 3, 0.75]} />
        <meshStandardMaterial attach='material' />
      </mesh>
      <mesh key='RightLeg' position={[0.5, -4.6, 0]} castShadow>
        <cylinderBufferGeometry attach='geometry' args={[0.4, 0.4, 3, 15]} />
        <meshStandardMaterial attach='material' />
      </mesh>
      <mesh key='LeftLeg' position={[-0.5, -4.6, 0]} castShadow>
        <cylinderBufferGeometry attach='geometry' args={[0.4, 0.4, 3, 15]} />
        <meshStandardMaterial attach='material' />
      </mesh>
    </group>
  );
}

const Arm = props => {
  const shoulderRef = useRef();
  const elbowRef = useRef();
  const wristRef = useRef();

  useRender(() => {
    if (props.sensors) {
      var shoulder = shoulderRef.current;
      var elbow = elbowRef.current;
      var wrist = wristRef.current;
      shoulder.rotation.x = props.sensors[0].x;
      shoulder.rotation.y = props.sensors[0].y;
      shoulder.rotation.z = props.sensors[0].z;
      elbow.rotation.x = shoulder.rotation.x - props.sensors[1].x;
      elbow.rotation.y = shoulder.rotation.y - props.sensors[1].y;
      elbow.rotation.z = shoulder.rotation.z - props.sensors[1].z;
      wrist.rotation.x =
        shoulder.rotation.x - elbow.rotation.x - props.sensors[2].x;
      wrist.rotation.y =
        shoulder.rotation.y - elbow.rotation.y - props.sensors[2].y;
      wrist.rotation.z =
        shoulder.rotation.z - elbow.rotation.z - props.sensors[2].z;
    }
  });

  return (
    <group ref={shoulderRef} position={[props.offset * props.side, -0.3, 0]}>
      <mesh castShadow>
        <sphereBufferGeometry attach='geometry' args={[0.4, 15, 15]} />
        <meshStandardMaterial attach='material' />
      </mesh>
      <mesh position={[0, (props.upperArmLength / 2) * -1, 0]} castShadow>
        <cylinderBufferGeometry
          attach='geometry'
          args={[0.4, 0.3, props.upperArmLength, 15]}
        />
        <meshStandardMaterial attach='material' />

        <group
          ref={elbowRef}
          position={[0, (props.upperArmLength / 2) * -1, 0]}
        >
          <mesh castShadow>
            <sphereBufferGeometry attach='geometry' args={[0.3, 15, 15]} />
            <meshStandardMaterial attach='material' />
          </mesh>
          <mesh position={[0, (props.lowerArmLength / 2) * -1, 0]} castShadow>
            <cylinderBufferGeometry
              attach='geometry'
              args={[0.3, 0.2, props.lowerArmLength, 15]}
            />
            <meshStandardMaterial attach='material' />

            <group
              ref={wristRef}
              position={[0, (props.lowerArmLength / 2) * -1, 0]}
            >
              <mesh castShadow>
                <sphereBufferGeometry attach='geometry' args={[0.2, 15, 15]} />
                <meshStandardMaterial attach='material' />
              </mesh>
              <mesh position={[0, (props.handLength / 2) * -1, 0]} castShadow>
                <cylinderBufferGeometry
                  attach='geometry'
                  args={[0.2, 0.15, props.handLength, 15]}
                />
                <meshStandardMaterial attach='material' />
              </mesh>
            </group>
          </mesh>
        </group>
      </mesh>
    </group>
  );
};
