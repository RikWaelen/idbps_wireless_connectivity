import React from 'react';
import { useLoader, useRender } from 'react-three-fiber';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { Math } from 'three';

export default function Gilbert(props) {
  const gltf = useLoader(GLTFLoader, 'Gilbert/scene.gltf');
  if (gltf) {
    gltf.__$[93].castShadow = true;
  }
  useRender(() => {
    if (gltf) {
      var shoulder = gltf.scene.getObjectByName('mixamorigLeftArm_09');
      var elbow = gltf.scene.getObjectByName('mixamorigLeftForeArm_010');
      var wrist = gltf.scene.getObjectByName('mixamorigLeftHand_011');
      shoulder.rotation.x = props.sensors[0].x;
      shoulder.rotation.y = props.sensors[0].y;
      shoulder.rotation.z = props.sensors[0].z;
      elbow.rotation.x = shoulder.rotation.x - props.sensors[1].x;
      elbow.rotation.y = shoulder.rotation.y - props.sensors[1].y;
      elbow.rotation.z = shoulder.rotation.z - props.sensors[1].z;
      // wrist.rotation.x =
      //   shoulder.rotation.x - props.sensors[2].x;
      // wrist.rotation.y =
      //   shoulder.rotation.y - elbow.rotation.y - props.sensors[2].y;
      // wrist.rotation.z =
      //   shoulder.rotation.z - elbow.rotation.z - props.sensors[2].z;
    }
  });

  return gltf ? (
    <primitive
      object={gltf.scene.getObjectByName('RootNode')}
      scale={[0.05, 0.05, 0.05]}
      position={[0, 0, 0]}
      rotation={[Math.degToRad(-90), 0, Math.degToRad(-90)]}
    />
  ) : null;
}
