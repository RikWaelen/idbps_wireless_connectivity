import React from 'react';
import logo from '../logo.svg';
import blelogo from '../Bluetooth logo.png';
import ConnectionOptions from '../BluetoothModule/ConnectionOptions';
import BluetoothModule from '../BluetoothModule/BluetoothModule';
import DataVisualisatie from '../UIComponents/Data-visualisatie';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.dataReceiverRef = React.createRef();
    this.bluetoothModule = new BluetoothModule();
    this.bluetoothModule.dataReceiverRef = this.dataReceiverRef;
    this.bluetoothInformation = this.bluetoothModule.deviceInformation;
    this.connectToCentralUnit = this.connectToCentralUnit.bind(this);
  }

  connectToCentralUnit() {
    let connectionOptions = new ConnectionOptions();
    var testMode = false;
    if (testMode) {
      this.dataReceiverRef.current.activateTestMode();
    } else {
      this.bluetoothModule.connectBluetoothDevice(connectionOptions);
    }
  }

  render = () => {
    return (
      <div id='root' className='App'>
        <header className='App-header HUD'>
          <div>
            <h1>Wireless iDBPS Demo</h1>
            <img src={logo} className='App-logo' alt='logo' />
          </div>
        </header>
        <div className='displayContainer innerbox'>
          <DataVisualisatie
            className='canvas'
            ref={this.dataReceiverRef}
          ></DataVisualisatie>
        </div>
        <button
          className='bluetoothButton HUD'
          onClick={this.connectToCentralUnit}
        >
          <img className='blelogo' src={blelogo} alt='xB' />
          <span>Connect</span>
        </button>
      </div>
    );
  };
}
export default Main;
