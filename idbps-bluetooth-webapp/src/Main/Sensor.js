class Sensor {
  constructor() {
    this.id = null;
    this.roll = null;
    this.pitch = null;
    this.yaw = null;
    this.x = null;
    this.y = null;
    this.z = null;
  }
}
export default Sensor;
