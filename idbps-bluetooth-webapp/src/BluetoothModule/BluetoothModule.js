import React from 'react';
import DeviceInformation from './DeviceInformation';

class BluetoothModule extends React.Component {
  constructor(props) {
    super(props);
    this.deviceInfo = new DeviceInformation();
    this.dataReceiverRef = null;
    this.bluetoothDevice = null;
    this.connectionOptions = null;
    this.connectBluetoothDevice = this.connectBluetoothDevice.bind(this);
    this.onIncommingMessage = this.onIncommingMessage.bind(this);
    this.exponentialBackoff = this.exponentialBackoff.bind(this);
  }
  connectBluetoothDevice(connectionOptions) {
    this.connectionOptions = connectionOptions;
    let options = this.createFilter(connectionOptions);

    navigator.bluetooth
      .requestDevice(options)
      .then(device => {
        return this.connectingDevice(
          device,
          connectionOptions.enableAutomaticReconnect
        );
      })
      .then(server => {
        return server.getPrimaryService(connectionOptions.serviceUUID);
      })
      .then(service => {
        return service.getCharacteristic(connectionOptions.characteristicUUID);
      })
      .then(characteristic => {
        this.createValueChangeEvents(
          characteristic,
          connectionOptions.enableNotifications
        );
        return characteristic.readValue();
      })
      .then(value => {
        this.parseCharacteristicValue(value);
      })
      .catch(error => {
        console.log(error);
      });
  }

  onIncommingMessage(event) {
    var value = event.target.value;
    var parsedValue = value.buffer ? value : new DataView(value);
    if (typeof this.dataReceiverRef.current.updateData === 'function') {
      this.dataReceiverRef.current.updateData(parsedValue);
    }
  }

  parseCharacteristicValue(value) {
    value = value.buffer ? value : new DataView(value);
  }

  createFilter(connectionOptions) {
    return {
      filters: [
        {
          name: connectionOptions.deviceNamesDetectionFilter
        }
      ],
      optionalServices: [
        connectionOptions.serviceUUID,
        connectionOptions.characteristicUUID
      ]
    };
  }

  connectingDevice(device, enableAutomaticReconnect) {
    if (
      typeof this.dataReceiverRef.current.receiveConnectedDevice() ===
      'function'
    ) {
      this.dataReceiverRef.current.receiveConnectedDevice(device);
    }
    this.bluetoothDevice = device;
    if (enableAutomaticReconnect) {
      device.addEventListener(
        'gattserverdisconnected',
        function(e) {
          this.onDisconnected(e);
        }.bind(this)
      );
    }
    this.deviceInfo.name = device.name;
    this.deviceInfo.id = device.id;
    console.log(device.name);
    console.log('Connecting Device');

    return this.bluetoothDevice.gatt.connect();
  }

  createValueChangeEvents(characteristic, enableNotifications) {
    if (enableNotifications === true) {
      characteristic.startNotifications();
      characteristic.addEventListener(
        'characteristicvaluechanged',
        this.onIncommingMessage
      );
    }
  }

  onDisconnected(event) {
    function toTry() {
      console.log(
        '> [' +
          new Date().toJSON().substr(11, 11) +
          '] ' +
          'Connecting to Bluetooth Device... '
      );
      this.startTime = new Date();
      return this.bluetoothDevice.gatt
        .connect()
        .then(server => {
          return server.getPrimaryService(
            '0000ffe0-0000-1000-8000-00805f9b34fb'
          );
        })
        .then(service => {
          return service.getCharacteristic(
            '0000ffe1-0000-1000-8000-00805f9b34fb'
          );
        })
        .then(characteristic => {
          this.createValueChangeEvents(
            characteristic,
            this.connectionOptions.enableNotifications
          );
          return characteristic.readValue();
        })
        .then(value => {
          this.parseCharacteristicValue(value);
        })
        .catch(error => {
          console.log(error);
        });
    }
    toTry = toTry.bind(this);
    function success() {
      console.log(
        '> [' +
          new Date().toJSON().substr(11, 11) +
          '] ' +
          'Bluetooth Device reconnected.'
      );
      console.log('> --------------------------- <');
    }
    function fail() {
      console.log(
        '> [' +
          new Date().toJSON().substr(11, 11) +
          '] ' +
          'Failed to reconnect.'
      );
    }
    let device = event.target;
    console.log(
      '> [' +
        new Date().toJSON().substr(11, 11) +
        '] ' +
        'Device ' +
        device.name +
        ' is disconnected.'
    );
    this.exponentialBackoff(
      3 /* max retries */,
      2 /* seconds delay */,
      toTry,
      success,
      fail
    );
  }

  exponentialBackoff(max, delay, toTry, success, fail) {
    toTry()
      .then(result => success(result))
      .catch(_ => {
        if (max === 0) {
          return fail();
        }
        console.log('Retrying in ' + delay + 's... (' + max + ' tries left)');
        setTimeout(function() {
          this.exponentialBackoff(--max, delay * 2, toTry, success, fail);
        }, delay * 1000);
      });
  }
}
export default BluetoothModule;
