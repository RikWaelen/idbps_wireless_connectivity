class ConnectionOptions {
  constructor() {
    this.deviceNamesDetectionFilter = ['HMSoft'];
    this.serviceUUID = '0000ffe0-0000-1000-8000-00805f9b34fb';
    this.characteristicUUID = '0000ffe1-0000-1000-8000-00805f9b34fb';
    this.enableNotifications = true;
    this.enableAutomaticReconnect = true;
    this.automaticReconnectAttempts = 3;
  }
}
export default ConnectionOptions;
