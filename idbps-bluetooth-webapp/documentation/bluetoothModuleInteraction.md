**How to use the bluetoothModule**

- import the bluetoothmodule and ConnectionOptions in your js file
  `import BluetootModule from './theLocationOfTheModule'`
  `import ConnectionOptions from './theLocationOfTheModule'`

- create a BluetoothModule object
  `this.bluetoothodule = new BluetoothModule()`

- pass a component ref into the bluetooth module
  - this component needs to have a function called getConnectedDevice(device)
    here you can save the device you ar connecting to
  - this component needs to have a function called updateData(data)
    here you can put the logic for the bytes you received from the bluetooth message

```
constructor(props){
    super(props)
    this.myRef = React.createRef();
    this.bluetootModule = new BluetoothModule();
    this.bluetoothModule.dataReceiverRef = this.myRef;
}
render(<MyComponent ref={this.myRef}>)
```

- create a ConnectionOptions object and set the following properties to something relevant for your Ble device

```
let connectionOptions = new ConnectionOptions();
```

connectionOptions defaults the following properties wich can be overridden

```
    connectionOptions.deviceNamesDetectionFilter = ['HMSoft'];
    connectionOptions.serviceUUID = '0000ffe0-0000-1000-8000-00805f9b34fb';
    connectionOptions.characteristicUUID = '0000ffe1-0000-1000-8000-00805f9b34fb';
    connectionOptions.enableNotifications = true;
    connectionOptions.enableAutomaticReconnect = true;
    connectionOptions.automaticReconnectAttempts = 3;
```

- call the following function from the bluetoothModule and pass the connectionOptions as the parameter
  `bluetoothModule.connectBluetoothDevice(connectionOptions);`
