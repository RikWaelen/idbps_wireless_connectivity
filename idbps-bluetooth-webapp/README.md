**Requirements**

Install NodeJS, React and Three

1 Install NodeJS

   - To get the latest version go to `https://nodejs.org/en/` and download the CURRENT version.
     - (optional) as of 06-12-2019 the latest version is downloadable here: `https://nodejs.org/dist/v13.3.0/node-v13.3.0-x64.msi`
   - NPM is installed with NodeJS
     - (optional) Check if you installed NodeJS and NPM in a terminal with `node -v` and `npm -v`

2 Install React

   - Run `npm i react` in a terminal
   - Run `npm i react-dom` in a terminal

3 Install Three.js

   - Run `npm i three` in a terminal (current version 0.112.1)
   - Run `npm i react-three-fiber` in a terminal (current version 3.0.17)
     - react three fiber is an interface for working with three.js components in react

**Running the application**

4 Run the application

   - Open a terminal in webapp folder
   - Run `npm start`
     - The app opens in your default browser. However bluetooth only works on `Google Chrome`, so open `localhost:3000` in chrome.
     
5 Hosted demo

  - the current application is hosted at `lifescience1.netlify.com`
